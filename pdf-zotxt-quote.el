;;; pdf-zotxt-quote.el --- Copy quotes from pdfs with correct page reference  -*- lexical-binding: t; -*-

;; Copyright (C) 2017-2019  Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Created: 2017-11-09
;; Modified: 2021-04-26
;; Version: 0.1
;; Keywords: convenience, wp
;; Package-Requires: ((emacs "25.1") (pdf-tools "0.70") (zotxt "6.0"))
;; URL: https://www.gitlab.com/andersjohansson/pdf-zotxt-quote

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; Defines a minor mode which overrides `pdf-view-kill-ring-save' in
;; `pdf-view-mode' to add org QUOTE-markers and an org-zotxt reference
;; with a correct page reference to copied text from pdfs. This is
;; done by setting two local variables `pdf-zotxt-quote-format' and
;; `pdf-zotxt-quote-page-setting'. These are then stored in a data file
;; (`pdf-zotxt-quote-data-file') so that they won’t need to be defined
;; for the same pdf file again.

;; If you want to activate it automatically with `pdf-view-mode'
;; (either via `pdf-view-mode-hook' or as a dir-local variable) use
;; `pdf-zotxt-quote-activate' since this will make it run after the
;; image is initialized.


;;; Code:

(require 'org-zotxt)
(require 'pdf-tools)
(require 'pdf-annot)
(require 'seq)
(require 'calc-stat)
(require 'pcase)

(defvar-local pdf-zotxt-quote-format ""
  "The link string used for references in the current file.
A single %s is replaced with the page number")

(defvar-local pdf-zotxt-quote-page-setting 1
  "How page numbers are interpreted for current file.
Can be the symbol ‘labels’ for using pagelabels stored in pdf, or
 a number (positive or negative) denoting the number of the first
 page.")

(defgroup pdf-zotxt-quote nil
  "Customization group for `pdf-zotxt-quote-mode'."
  :group 'zotxt)

(defcustom pdf-zotxt-quote-data-file
  (if (functionp 'no-littering-expand-var-file-name)
      (no-littering-expand-var-file-name
       "pdf-zotxt-quote/data.el")
    (expand-file-name "pdf-zotxt-quote-data.el" user-emacs-directory))
  "Data file for storing quote format and page offset for files."
  :type 'filename)

(defcustom pdf-zotxt-quote-data-file-limit 1000
  "Limit for number of items to be kept in datafile."
  :type '(choice (integer :tag "On") (const :tag "Off" -1)))

(defcustom pdf-zotxt-quote-wrapper "#+begin_quote\n%s\n%s\n#+end_quote\n"
  "Format for wrapping quote and citation.
Two %s should be included in the string for quote and citation
respectively.

Standard format is an `org-mode' quote."
  :type 'string)
(put 'pdf-zotxt-quote-wrapper 'safe-local-variable #'stringp)

(defcustom pdf-zotxt-quote-fill-paragraphs t
  "If non-nil, fill paragraphs in copied string."
  :type 'boolean)

(defcustom pdf-zotxt-quote-fill-column nil
  "Fill column to use if filling paragraphs.
\(depending on ‘pdf-zotxt-quote-fill-paragraphs’).
Nil means using default ‘fill-column’."
  :type '(choice integer (const :tag "Use default" nil)))

(defcustom pdf-zotxt-quote-normalize-paragraphs t
  "If non-nil, try to create correct paragraphs in copied string.
Only activated if ‘pdf-zotxt-quote-fill-paragraphs’ is active.
This uses some heuristics for guessing what is a paragraph
break (assuming things like paragraph indent). See
‘pdf-zotxt-quote--normalize-paragraphs’ for details."
  :type 'boolean)

(defcustom pdf-zotxt-quote-unhyphenate t
  "If non-nil, merge hyphenated words at end of line.
Only activated if ‘pdf-zotxt-quote-fill-paragraphs’ is active.
See ‘pdf-zotxt-quote--unhyphenate’, for details."
  :type 'boolean)

(defcustom pdf-zotxt-quote-replace-ligatures t
  "If non-nil, replaces ligatures like ﬁ with the characters f and i.
See ‘pdf-zotxt-quote-ligatures’ for the ligatures to remove."
  :type 'boolean)

(defcustom pdf-zotxt-quote-ligatures
  '(("ﬁ" . "fi")
    ("ﬃ" . "ffi")
    ("ﬀ" . "ff")
    ("ﬂ" . "fl")
    ("ﬄ" . "ffl")
    ("‐" . "-") ; hyphen → hyphen-minus
    )
  "Ligatures to replace if ‘pdf-zotxt-quote-replace-ligatures’ is non-nil.
Alist of cells (string . string) like (\"ﬁ\" . \"fi\").
Note that you could put in whatever string replacements you like here."
  :type '(alist :key-type string :value-type string))


;;;; Define mode and activation functions
;;;###autoload
(define-minor-mode pdf-zotxt-quote-mode
  "Minor mode for copying quotes from pdfs with zotero-linked references."
  nil
  " PZQ"
  nil
  (when pdf-zotxt-quote-mode
    (condition-case nil
        (pdf-zotxt-quote-acquire-variables)
      (quit (pdf-zotxt-quote-mode -1))
      (error (message "pdf-zotxt-quote-mode, error in acquiring variables.")
             (pdf-zotxt-quote-mode -1)))))

;;;###autoload
(define-minor-mode pdf-zotxt-quote-cleanup-mode
  "Minor mode for cleaning up text when copying from pdf files.

Does the same cleanup as ‘pdf-zotxt-quote-mode’, according to its options
(see customize group pdf-zotxt-quote)")

;;;###autoload
(defun pdf-zotxt-quote-activate ()
  "Activate the mode after ‘image-mode’ does it’s thing.

Use this when activating through `pdf-view-mode-hook' so that the
first page is visible when input is asked for."
  (add-hook 'image-mode-new-window-functions #'pdf-zotxt-quote--activate-1 t t))

(defun pdf-zotxt-quote--activate-1 (&rest _ignore)
  "Internal activation function run in hook."
  (unwind-protect
      (unless pdf-zotxt-quote-mode (pdf-zotxt-quote-mode t))
    (remove-hook 'image-mode-new-window-functions #'pdf-zotxt-quote--activate-1 t)))

;;;; Functions for getting and setting variables, and storing them in file
(defun pdf-zotxt-quote-acquire-variables (&optional new dont-ask)
  "Acquire format and page variables for this buffer.
Attempt to load from ‘pdf-zotxt-quote-data-file’, if
unsuccesful (or prefix arg NEW is given) query the user. If
DONT-ASK is non-nil, just return nil if no stored variables were
found."
  (interactive "P")
  (let* ((data (pdf-zotxt-quote--get-file-data))
         (found (assoc (buffer-file-name) data)))
    (cond ((or new (and (not dont-ask) (not found)))
           (pdf-zotxt-quote-set-page-setting)
           (pdf-zotxt-quote-set-quote-format data))
          ((and (not found) dont-ask)
           nil)
          (t (setq pdf-zotxt-quote-format (nth 1 found)
                   pdf-zotxt-quote-page-setting (nth 2 found))))))

(defun pdf-zotxt-quote-set-page-setting (&optional setting)
  "Set ‘pdf-zotxt-quote-page-setting’, to numeric prefix arg SETTING or query response."
  (interactive "P")
  (setq pdf-zotxt-quote-page-setting
        (or (when (or (integerp setting) (eq 'labels setting)) setting)
            (let ((setting
                   (read-string "Page setting (number or ‘labels’) "
                                (if (equal "1" (car (pdf-info-pagelabels))) "1" "labels"))))
              (if (equal "labels" setting)
                  'labels
                (round (string-to-number setting))))))
  ;;make sure to store the new value when called interactively:
  (when (called-interactively-p 'any) (pdf-zotxt-quote--write-file-data (pdf-zotxt-quote--get-file-data))))

(defun pdf-zotxt-quote-set-quote-format (&optional current-data)
  "Retrieve the reference format from zotxt and set ‘pdf-zotxt-quote-format’.
Insert escape for page reference.
If CURRENT-DATA is passed, use this as basis for data stored in ‘pdf-zotxt-quote-data-file’."
  (interactive)
  (let ((pdf-buf (current-buffer))
        (tempbuf (get-buffer-create " *pdf-zotxt-tmp*")))
    (deferred:$
      (zotxt-choose-deferred zotxt-default-search-method)
      (deferred:nextc it
        (lambda (items)
          (if (null items)
              (error "No item found for search")
            (zotxt-mapcar-deferred #'org-zotxt-get-item-link-text-deferred items))))
      (deferred:nextc it
        (lambda (items)
          (with-current-buffer tempbuf
            (delete-region (point-min) (point-max)) ;; instead of
            ;; killing buffer
            (org-zotxt-insert-reference-link-to-item (car items))
            (goto-char (- (point) 2))
            (insert ", p. %s")
            (let ((f (buffer-string)))
              (with-current-buffer pdf-buf
                (setq pdf-zotxt-quote-format f)
                ;; always store data here:
                (pdf-zotxt-quote--write-file-data (or current-data (pdf-zotxt-quote--get-file-data))))))
          ;; (kill-buffer tempbuf) ; always returns error (Marker does not point anywhere) later...
          ))
      (deferred:error it
        (lambda (err)
          (error (concat "pdf-zotxt-quote error: " (error-message-string err)))))
      (if zotxt--debug-sync (deferred:sync! it)))))


(defun pdf-zotxt-quote--get-file-data ()
  "Read data from ‘pdf-zotxt-quote-data-file’."
  (if (file-readable-p pdf-zotxt-quote-data-file)
      (with-temp-buffer
        (insert-file-contents pdf-zotxt-quote-data-file)
        (read (current-buffer)))
    (if (y-or-n-p (format "Create ‘pdf-zotxt-quote-data-file’ (%s)? " pdf-zotxt-quote-data-file))
        (progn
          (make-directory (file-name-directory pdf-zotxt-quote-data-file) t)
          (with-temp-file pdf-zotxt-quote-data-file (insert "()"))
          (pdf-zotxt-quote--get-file-data)) ;; TODO, don’t we know
      ;; that it’s empty here as we’ve just created it??
      (error (format "No file (%s) to store pdf-zotxt-quote data in." pdf-zotxt-quote-data-file)))))

(defun pdf-zotxt-quote--write-file-data (data)
  "Write DATA to ‘pdf-zotxt-quote-data-file’, adding data for current buffer."
  (let* ((bfn (buffer-file-name))
         (data (cl-delete-if
                (lambda (x) (equal bfn (car x)))
                data)))
    (push (list (buffer-file-name)
                pdf-zotxt-quote-format
                pdf-zotxt-quote-page-setting)
          data)
    (if (file-writable-p pdf-zotxt-quote-data-file)
        (with-temp-file pdf-zotxt-quote-data-file
          (let ((print-level nil)
                (print-length nil))
            (insert (format ";;; -*- coding: %s -*-\n"
                            (symbol-name buffer-file-coding-system)))
            (pp
             (if (< 0 pdf-zotxt-quote-data-file-limit)
                 (seq-take data pdf-zotxt-quote-data-file-limit)
               data)
             (current-buffer))))
      (user-error "File not writable %s" pdf-zotxt-quote-data-file))))

;;;; Function to jump to page-or-label
(defun pdf-zotxt-quote-goto-page (page-or-label)
  "Jump to page PAGE-OR-LABEL in current buffer pdf.
Take settings from ‘pdf-zotxt-quote-mode’ into account."
  (interactive "s")
  (when (pdf-zotxt-quote-acquire-variables)
    (if (eq pdf-zotxt-quote-page-setting 'labels)
        (pdf-view-goto-label page-or-label)
      (pdf-view-goto-page (1+ (- (if (stringp page-or-label)
                                     (string-to-number page-or-label)
                                   page-or-label)
                                 pdf-zotxt-quote-page-setting))))))


;;;; Define kill-override function for formatting quotes
(defun pdf-zotxt-quote-kill-override (&optional arg)
  "Add page reference and QUOTE-tags to the killed string.
Does a normal kill if given a prefix argument ARG."
  (interactive "P")
  (pdf-view-assert-active-region)
  (let* ((txt (pdf-view-active-region-text))
         (txt (mapconcat 'identity txt "\n"))
         (txt (cond ((and (not arg) pdf-zotxt-quote-mode pdf-zotxt-quote-format)
                     (pdf-zotxt-quote--format-quote
                      txt
                      (pdf-zotxt-quote--adjusted-page
                       (pdf-view-current-page))))
                    ((and (not arg) pdf-zotxt-quote-cleanup-mode)
                     (pdf-zotxt-quote--cleanup-text txt))
                    (t txt))))
    (pdf-view-deactivate-region)
    (kill-new txt)))

(advice-add 'pdf-view-kill-ring-save :override #'pdf-zotxt-quote-kill-override)

;;;; Formatting functions
(defun pdf-zotxt-quote--format-quote (text page)
  "Format TEXT with a citation and page reference to PAGE.

Uses ‘pdf-zotxt-quote-wrapper’. Optionally cleans up
text (depending on ‘pdf-zotxt-quote-fill-paragraphs’,
‘pdf-zotxt-quote-normalize’, ‘pdf-zotxt-quote-unhyphenate’, and
‘pdf-zotxt-quote-replace-ligatures’)."
  (format pdf-zotxt-quote-wrapper
          (replace-regexp-in-string
           "%" "%%"
           (pdf-zotxt-quote--cleanup-text text))
          (format pdf-zotxt-quote-format page)))

(defun pdf-zotxt-quote--adjusted-page (physical-page)
  "Return the logical page as a string.
The page is deduced from PHYSICAL-PAGE and ‘pdf-zotxt-quote-page-setting’."
  (pcase pdf-zotxt-quote-page-setting
    ((pred integerp)
     (number-to-string (1- (+ pdf-zotxt-quote-page-setting physical-page))))
    ('labels
        (nth (1- physical-page) (pdf-info-pagelabels)))
    (_ (user-error "Invalid ‘pdf-zotxt-quote-page-setting’"))))


(defun pdf-zotxt-quote--cleanup-text (txt)
  "Clean up text according to settings.
Runs operations choosen by ‘pdf-zotxt-quote-fill-paragraphs’
‘pdf-zotxt-quote-normalize-paragraphs’,
‘pdf-zotxt-quote-unhyphenate’,
‘pdf-zotxt-quote-replace-ligatures’ and returns modified TXT."
  (if (or pdf-zotxt-quote-fill-paragraphs pdf-zotxt-quote-replace-ligatures)
      (with-temp-buffer
        (text-mode) ; user may have settings that should apply to
                                        ; fill etc. set as text-mode hooks
        (let ((fill-column (or pdf-zotxt-quote-fill-column fill-column)))
          (insert txt)
          (when pdf-zotxt-quote-replace-ligatures
            (pdf-zotxt-quote--replace-ligatures))
          (when pdf-zotxt-quote-fill-paragraphs
            (when pdf-zotxt-quote-normalize-paragraphs
              (pdf-zotxt-quote--normalize-paragraphs))
            (when pdf-zotxt-quote-unhyphenate
              (pdf-zotxt-quote--unhyphenate))
            (fill-region (point-min) (point-max)))
          (buffer-string)))
    txt))

(defun pdf-zotxt-quote--replace-ligatures ()
  "Replace ligatures defined in ‘pdf-zotxt-quote-replace-ligatures’ in current buffer."
  (cl-loop for (lig . rep) in pdf-zotxt-quote-ligatures do
           (goto-char (point-min))
           (while (search-forward lig nil t)
             (replace-match rep t t))))

(defun pdf-zotxt-quote--normalize-paragraphs ()
  "Insert line breaks to mark off paragraphs in current buffer.
Assume that copied pdf string is from justified text with
indented paragraps, but that indent is not included in copied
text. Infer paragraph breaks by finding “short” lines."
  (let (lines limit)
    (goto-char (point-min))
    (while (eq 0 (forward-line))
      (let ((peob (point)))
        (end-of-line)
        (unless (eq (point-max) (point))
          (push (cons (point-marker) (- (point) peob)) lines))))
    ;; remove zero-lenght lines
    (setq lines (cl-delete-if #'zerop lines :key #'cdr))
    (when (< 3 (length lines))
      (setq limit (* 0.8
                     (string-to-number
                      (math-format-number
                       (apply #'calcFunc-vmean (mapcar #'cdr lines))))))
      (dolist (l lines)
        (when (< (cdr l) limit)
          (goto-char (car l))
          (newline))))))

(defun pdf-zotxt-quote--unhyphenate ()
  "Merge hyphenated words at end of line in current buffer."
  (goto-char (point-min))
  (while (search-forward "-\n" nil t)
    (replace-match "")))

;;;; Get all annotations in file
;;;###autoload
(defun pdf-zotxt-quote-get-all-annots-in-buffer-or-file (&optional filename)
  "Put all annotations (highlights etc.) as formatted quotes in the kill ring.

Prompts for a file if current buffer is not a pdf.
A non-interactive call can pass a pdf FILENAME to scan."
  (interactive)
  (let* ((kill t)
         (buf (or
               (when filename
                 (if-let ((buf (find-buffer-visiting filename)))
                     (progn (setq kill nil)
                            buf)
                   (find-file-noselect filename)))
               (when (pdf-util-pdf-buffer-p (current-buffer))
                 (setq kill nil)
                 (current-buffer))
               (find-file-noselect
                (read-file-name "File to extract annotations from: "
                                nil nil 'confirm nil
                                (lambda (fn)
                                  (string= "pdf" (file-name-extension fn))))))))
    (save-window-excursion
      ;; maybe not the recommended way, but we need it visible for
      ;; setting page numbers etc.
      (switch-to-buffer-other-window buf)
      (pdf-zotxt-quote-acquire-variables)
      (kill-new (pdf-zotxt-quote--get-all-annots)))
    (when kill (kill-buffer buf))))

(defun pdf-zotxt-quote--get-all-annots ()
  "Get all annotations from pdf in current buffer."
  (mapconcat #'pdf-zotxt-quote--format-annot
             (sort (pdf-annot-getannots nil '(text squiqqly highlight underline strike-out)
                                        (current-buffer))
                   'pdf-annot-compare-annotations)
             "\n\n"))

(defun pdf-zotxt-quote--format-annot (annotation)
  "Format pdf ANNOTATION for org-output."
  (let* ((contents (pdf-annot-get annotation 'contents))
         (page (pdf-annot-get annotation 'page "??"))
         (adj-page (pdf-zotxt-quote--adjusted-page page))
         (type (pdf-annot-get annotation 'type)))
    (concat
     (when (not (string-blank-p contents))
       (concat "/" contents "/"
               (if (eq type 'text)
                   (format " [[pdfview:%s::%s][(p. %s)]]"
                           (buffer-file-name (pdf-annot-get annotation 'buffer))
                           page ; org-pdfview links to physical pages
                           adj-page) ;but we include the logical in description
                 ":\n")))
     (when (memq type '(highlight squiqqly underline strike-out))
       (pdf-zotxt-quote--format-quote
        (pdf-info-gettext
         page
         (pdf-zotxt-quote--edges-to-region (pdf-annot-get annotation 'markup-edges))
         nil
         (pdf-annot-get annotation 'buffer))
        adj-page)))))

;; From https://matt.hackinghistory.ca/2015/11/11/note-taking-with-pdf-tools/
;; https://github.com/pinguim06/pdf-tools/commit/22629c746878f4e554d4e530306f3433d594a654
(defun pdf-zotxt-quote--edges-to-region (edges)
  "Attempt to get 4-entry region \(left top right bottom\) from several EDGES.

We need this to import annotations and to get marked-up text,
because annotations are referenced by its edges, but functions
for these tasks need region."

  (let ((left0 (nth 0 (car edges)))
        (top0 (nth 1 (car edges)))
        (bottom0 (nth 3 (car edges)))
        (top1 (nth 1 (car (last edges))))
        (right1 (nth 2 (car (last edges))))
        (bottom1 (nth 3 (car (last edges)))))
    ;; we try to guess the line height to move
    ;; the region away from the boundary and
    ;; avoid double lines
    (list left0
          (+ top0 (/ (- bottom0 top0) 2))
          right1
          (- bottom1 (/ (- bottom1 top1) 2 )))))


(provide 'pdf-zotxt-quote)
;;; pdf-zotxt-quote.el ends here
